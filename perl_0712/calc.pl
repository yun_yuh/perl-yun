#!/usr/bin/env perl

use strict;
use warnings;
#use MathBig::Int;

print 'first:';
my $first = <STDIN>;
chomp $first;
print 'second:';
my $second = <STDIN>;
chomp $second;
my $res;


$res = $first + $second;
print "$res\n";
$res = $first - $second;
print "$res\n";
$res = $first * $second;
print "$res\n";
$res = $first / $second;
print "$res\n";
$res = $first ** $second;
print "$res\n";
