#!/usr/bin/env perl

use strict;
use warnings;

my $start = 1;
my $end = 100;

for my $i ($start .. $end) {

if($i % 3 == 0 && $i % 5 == 0) {
print "FizzBuzz\n";
}
elsif($i % 3 == 0) {
print "Fizz\n";
}
elsif($i % 5 == 0) {
print "Buzz\n";
}
else {
print "$i\n";
}


}
