#!/usr/bin/env perl

use strict;
use warnings;
#use MathBig::Int;

my $true = int(rand(100));

while (1) {
    my $ans = <STDIN>;
    chomp $ans;
    if($ans == $true) {
        print "正解です\n";
        last;
    }
    elsif($ans > $true && $ans < ($true + 5) ) {
        print "近い。\n";
        next;
    }
    elsif($ans > $true) {
        print "$ansより下です。\n";
        next;
    }
    elsif($ans < $true && $ans > ($true - 5) ) {
        print "近い。\n";
        next;
    }
    elsif($ans < $true) {
        print "$ansより上です。\n";
        next;
    }
   else {
        last;
    }
}
