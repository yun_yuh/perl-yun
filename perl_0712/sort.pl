#!/usr/bin/env perl

use strict;
use warnings;
my @array;

for my $i (0..2) {
    $array[$i] = <STDIN>;
    chomp $array[$i];
}

my @array2 = sort {$a cmp $b}@array;

print "------------------\n";
for my $ans (@array2) {
    print "$ans\n";
}
