#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;

my @array = qw( 1 2 3); #個人的にqwで書く練習
my %hash = (a => 1, b => 2);
my @ref_array = (\@array,\%hash,'c');

print @array , "\n"; 
print @ref_array , "\n";
print $ref_array[0]->[1]."\n";
print $ref_array[1]->{a}."\n";

#一度にリファレンスとデータを書く,$で受けれる
my $array_ref = ['aaa','bbb','ccc'];
my $hash_ref = {a => 1, b => 2};
my @array3 = ($array_ref, $hash_ref, 'c');
print $array3[0]->[2]."\n"; #ccc
print $array3[1]->{'b'}."\n"; #2
#全体のデータが欲しい場合、デリファレンスする
print $array_ref, "\n"; #アドレスが出る
print @$array_ref, "\n"; #リファレンスの頭に配列であれば@をつける
print %$hash_ref, "\n"; #リファレンスの頭にハッシュであれば#をつける



#サブルーチン
sub say {
    my $str = shift @_;
    print "$str\n";
}

say("hello,world");
say('');

sub same {
my ($i, $j) = @_;
    if($i eq $j) {
        say('same');
    }
    else {
        say('not same');
    }
}

same('a','a');
same('a','b');

#practice
sub add {
my ($num1,$num2) = @_;
return ($num1+$num2);
}
say(add(1,2));

sub min {
my ($num3,$num4) = @_;
return ($num3-$num4);
}
say(min(3,2));

sub mul {
my ($num5,$num6) = @_;
return ($num5 * $num6);
}
say(mul(2,5));

sub div {
my ($num7,$num8) = @_;
return ($num7 / $num8);
}
say(div(6,3));


#practice
my @arrayX = ('x', 'y', 'z' );
my %hashX = ( lang => 'japanese' , location => 'japan' );

sub output_array_and_hash {
    my ($numA,$numB) = @_;
    print Dumper $numA;
    print Dumper $numB;
}

output_array_and_hash(\@arrayX,\%hashX);




#正規表現
#my $ans = 'abcdEfGhijKlmn';
sub pattern {
    my $ans = shift;
    print "return OK if you type F,G or I.\n";
    if ($ans =~ /[FGI]/) {
        print "OK\n";
    }
    else {
        print "NG\n";
    }
}

#print "type 'exit' if you want to stop program\n";
#while(chomp (my $arg = <STDIN>)) {
#    if($arg eq 'exit'){exit;}
#    pattern($arg);
#}

sub perl_checker {
    my ($arg) = shift;
    my $pattern = qr/[pP]erl/;
#    if ($arg =~ /[pP]erl/) {
     if ($arg =~ $pattern) { #変数の場合、qrじゃないと正規表現が使えない
    print "Perl Monger!\n";
    }
}

perl_checker('ttt');
perl_checker('perl');
perl_checker('pPerl');


#practice
my $words_ref = [
    'papix loves meat!',
    'boolfool loves sushi!',
];
sub love_food {
    my $arg = shift;
    my $pattern = qr/(.*)\sloves\s(.*)/;
    foreach my $val (@$arg) {
        if ($val =~ $pattern) {
            print "$1 -> $2\n";
        }
        else {
            "No\n";
        }
    }
}
love_food($words_ref);


#practice
my $str = 'I love ruby';
    $str =~ s/ruby/perl/g;
print "$str\n";
