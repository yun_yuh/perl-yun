#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
my $foods = ['yakiniku', 'sushi'];
my $ref = {name => 'yun' , favorite_foods => $foods };

#print Dumper $ref;
#print $ref->{'name'};
#print ref $ref->{'favorite_foods'},"\n";

foreach my $key (keys %$ref) {
    if(ref $ref->{$key} ne 'ARRAY'){
        print $key. " = " . $ref->{$key} ."\n";
    }
    else {
        my $test = $ref->{$key};
        print "$key = ";
        foreach my $value (@$test) {
            print "$value ";
        }
        print "\n";
    }
}
